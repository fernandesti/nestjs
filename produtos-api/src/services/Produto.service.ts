import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { Produto } from "src/models/Produto";

@Injectable()
export class ProdutoService {

    constructor(
        @InjectModel(Produto)
        private produtoModel: typeof Produto
    ){}

    // mock: Produto[] = [];
    //     new Produto("PS5", 4.500),
    //     new Produto("Notebook", 5.000),
    //     new Produto("TV", 2.500)
    // ];


    async TodosProdutos(){
        return this.produtoModel.findAll();
    }

    async Produto(id: string){
        return this.produtoModel.findByPk(id);
    }

    async adicionarProduto(produto: Produto){
        return this.produtoModel.create(produto);
    }

    async modificarProduto(produto: Produto): Promise<[Number, Produto[]]>{
        return this.produtoModel.update(produto, {
            where:{
                id : produto.id
            }
        })
    }

    async deletarProduto(id: string){
        const p = await this.Produto(id);
        return await p.destroy();
    }
}