import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put } from "@nestjs/common";
import { Produto } from "src/models/Produto";
import { ProdutoService } from "src/services/Produto.service";


@Controller('produtos')
export class ProdutosController {

    constructor(private produtoService: ProdutoService){}

    @Get()
    todosProdutos() :Promise<Produto[]> {
        return this.produtoService.TodosProdutos();
    }

    @Get(':id')
    produto(@Param() params) :Promise<Produto> {
        return this.produtoService.Produto(params.id);
    }

    @Post()
    @HttpCode(204)
    adicionarProdutos(@Body() params) :Promise<Produto> {
        console.log(params);
        return this.produtoService.adicionarProduto(params);
    }

    @Put()
    modificarProduto(@Body() params) :Promise<[Number, Produto[]]> {
        console.log(params);
        return this.produtoService.modificarProduto(params);
    }

    @Delete(':id')
    @HttpCode(204)
    deletarProduto(@Param() params) {
        return this.produtoService.deletarProduto(params.id);
    }
}